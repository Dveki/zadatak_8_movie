<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Movies On TV</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="style/style.css" />
    </head>
    <body>
        <div class="wrapper">
        <?php 
        include("inc/header.html"); 
        include("inc/aside.html"); 
        
        if(!isset($_GET["link"])) 
        $_GET["link"]="index"; 
        
        switch ($_GET["link"]) {   
            
            case "film":  
            include("inc/film.php");  
            break;  
            
            case "genre":  
            include("inc/genre.php");  
            break;  
            
            case "schedule":  
            include("inc/schedule.php");  
            break; 

            case "form":  
            include("inc/form.php");  
            break;
            
            default:  
            include("inc/film.php");  
            break; 

            } 
        include("inc/footer.html"); ?>
        </div>
    </body>
</html>